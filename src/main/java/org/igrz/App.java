package org.igrz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.concurrent.TimeUnit;

public class App {
    public static void main(String[] args) throws Exception {
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test", "moysklad", "moysklad")) {
            System.out.println("Connected");

            int i = 0;
            while (true) {
                try (PreparedStatement st = conn.prepareStatement(String.valueOf((BIG_SQL_TEXT).toCharArray()))) {
                    st.setQueryTimeout((int) TimeUnit.SECONDS.toMillis(1));
                    st.execute();
                }
                ++i;
                if (i % 10000 == 0) {
                    System.out.println("Executed 10000 statements");
                }
            }
        }
    }

    private static final String BIG_SQL_TEXT =
            "/*-- Еh bien, mon prince. Gênes et Lucques ne sont plus que des apanages,\n" +
            "  des поместья, de la famille Buonaparte.  Non, je  vous préviens, que si vous\n" +
            "  ne  me dites pas, que nous avons la guerre, si vous vous permettez encore de\n" +
            "  pallier  toutes les infamies, toutes les  atrocités  de cet  Antichrist  (ma\n" +
            "  parole, j'y  crois) -- je  ne  vous  connais plus, vous n'êtes plus mon ami,\n" +
            "  vous n'êtes  plus  мой  верный  раб,  comme  vous  dites.  [1]  Ну,\n" +
            "  здравствуйте, здравствуйте.  Je vois  que  je  vous fais  peur, [2]\n" +
            "  садитесь и рассказывайте.\n" +
            "       Так говорила в июле 1805 года известная Анна Павловна Шерер, фрейлина и\n" +
            "  приближенная  императрицы  Марии  Феодоровны,  встречая важного и  чиновного\n" +
            "  князя  Василия,  первого  приехавшего  на  ее вечер. Анна  Павловна  кашляла\n" +
            "  несколько  дней, у  нее был грипп, как она говорила (грипп  был тогда  новое\n" +
            "  слово, употреблявшееся только  редкими).  В записочках, разосланных  утром с\n" +
            "  красным лакеем, было написано без различия во всех:\n" +
            "       \"Si vous n'avez rien de mieux à faire, M. le comte (или mon prince), et\n" +
            "  si la perspective de passer la soirée chez une pauvre malade ne vous effraye\n" +
            "  pas  trop,  je serai charmée de vous  voir chez moi  entre 7 et  10  heures.\n" +
            "  Annette Scherer\".[3]\n" +
            "       -- Dieu, quelle virulente sortie [4] -- отвечал,  нисколько не\n" +
            "  смутясь  такою встречей, вошедший  князь,  в  придворном, шитом  мундире,  в\n" +
            "  чулках,  башмаках,  при  звездах,  с  светлым выражением плоского  лица.  Он\n" +
            "  говорил на том изысканном французском языке, на  котором не только говорили,\n" +
            "  но и  думали  наши  деды, и с теми тихими, покровительственными интонациями,\n" +
            "  которые  свойственны  состаревшемуся  в  свете  и  при  дворе  значительному\n" +
            "  человеку. Он подошел к Анне  Павловне, поцеловал ее руку,  подставив ей свою\n" +
            "  надушенную и сияющую лысину, и покойно уселся на диване.\n" +
            "       -- Avant tout  dites moi, comment vous  allez, chère amie? [5]\n" +
            "  Успокойте друга, -- сказал он,  не  изменяя голоса и тоном, в котором  из-за\n" +
            "  приличия и участия просвечивало равнодушие и даже насмешка.*/" +
            " select 1";
}
